import axios from 'axios';
import { getConfig } from './config'


export var searchUser = (data) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `{getUsers(mobileNo:"${data.mobileNo}",level:"${data.level}",page:${data.page}){_id mobileNo id status level promotedBy{_id mobileNo id status level}}}`,
            variables: null
        }
        axios.post(`/graphql/user?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.getUsers);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
export var promoteTo1 = (_id) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `mutation{promoteTo1(_id:"${_id}"){_id mobileNo id status level promotedBy{_id mobileNo id status level}}}`,
            variables: null
        }
        axios.post(`/graphql/user?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.promoteTo1);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
export var promoteTo2 = (_id) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `mutation{promoteTo2(_id:"${_id}"){_id mobileNo id status level promotedBy{_id mobileNo id status level}}}`,
            variables: null
        }
        axios.post(`/graphql/user?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.promoteTo2);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
