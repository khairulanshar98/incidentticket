function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}

function getTime(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

export function timeSince(dateEpoc) {
    var date = new Date(dateEpoc);
    var now = new Date();
    var now = now.getTime() + (now.getTimezoneOffset() * 60000)
    var date_ = date.getTime() + (date.getTimezoneOffset() * 60000)
    var seconds = Math.floor((now - date_) / 1000);
    //var interval = Math.floor(seconds / 31536000);

    /*if (interval > 1) {
      return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + " months";
    }*/
    if (seconds <= 60) {
        return Math.floor(seconds) + ' seconds ago';
    }

    interval = Math.floor(seconds / 60);
    if (interval <= 60) {
        return interval + ' minutes ago';
    }

    interval = Math.floor(seconds / 3600);
    if (interval <= 24) {
        return interval + ' hours ago';
    }
    var interval = Math.floor(seconds / 86400);
    if (interval >= 1 && interval < 2) {
        return interval + ' days ago ' + getTime(date);
    }
    if (interval >= 2) {
        return formatDate(date);
    }
}