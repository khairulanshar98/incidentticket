import axios from 'axios';
import { getConfig } from './config'

export var graphToken = () => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `{getToken{csrf status user {_id mobileNo id status level csrf} kit{appId version csrf} }}`,
            variables: null
        }
        axios.post(`/graphql/token?`, input, getConfig())
            .then(response => {
                if (response.headers.csrf)
                    localStorage.setItem('csrf', response.headers.csrf);
                var data = response.data.data.getToken;
                resolve(data);
            }).catch(err => {
                resolve(null)
            })
    });
};
export var loginAccountKit = (resp) => {
    return new Promise((resolve, reject) => {
        axios.post('/api/login/phone', { token: resp.code }, getConfig())
            .then(response => {
                if (response.headers.csrf)
                    localStorage.setItem('csrf', response.headers.csrf)
                resolve(response.data);
            }).catch(err => {
                resolve(null)
            })
    });
};

export var onLogout = () => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `{clearToken{csrf status kit{appId version csrf}}}`,
            variables: null
        }
        axios.post(`/graphql/token?`, input, getConfig())
            .then(response => {
                if (response.headers.csrf)
                    localStorage.setItem('csrf', response.headers.csrf);
                var data = response.data.data.clearToken;
                resolve(data);
            }).catch(err => {
                resolve(null)
            })
    });
};