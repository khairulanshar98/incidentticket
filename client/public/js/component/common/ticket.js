import axios from 'axios';
import { getConfig } from './config'


export var searchTicket = (data) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `{getTickets(title:"${data.title}",description:"${data.description}",status:"${data.status}",sort:"${data.sort}",descending:${data.descending ? -1 : 1},page:${data.page}){_id title description status createdBy{_id mobileNo id status level} createdAt assignedTo{_id mobileNo id status level} assignedAt acknowledgedAt resolvedAt}}`,
            variables: null
        }
        axios.post(`/graphql/ticket?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.getTickets);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
export var createTicket = (data) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `mutation{createTicket(input:{title:"${data.title}",description:"${encodeURIComponent(data.description)}"}){_id title description status createdBy{_id mobileNo id status level} createdAt assignedTo{_id mobileNo id status level} assignedAt acknowledgedAt resolvedAt}}`,
            variables: null
        }
        axios.post(`/graphql/ticket?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.createTicket);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
export var assignTo = (_id, user_id) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `mutation{assignTo(_id:"${_id}",user_id:"${user_id}"){_id title description status createdBy{_id mobileNo id status level} createdAt assignedTo{_id mobileNo id status level} assignedAt acknowledgedAt resolvedAt}}`,
            variables: null
        }
        axios.post(`/graphql/ticket?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.assignTo);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
export var acknowledge = (_id) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `mutation{acknowledge(_id:"${_id}"){_id title description status createdBy{_id mobileNo id status level} createdAt assignedTo{_id mobileNo id status level} assignedAt acknowledgedAt resolvedAt}}`,
            variables: null
        }
        axios.post(`/graphql/ticket?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.acknowledge);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
export var resolve = (_id) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `mutation{resolve(_id:"${_id}"){_id title description status createdBy{_id mobileNo id status level} createdAt assignedTo{_id mobileNo id status level} assignedAt acknowledgedAt resolvedAt}}`,
            variables: null
        }
        axios.post(`/graphql/ticket?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.resolve);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
export var deleteTicket = (_id) => {
    return new Promise((resolve, reject) => {
        var input = {
            query: `mutation{deleteTicket(_id:"${_id}"){_id title description status createdBy{_id mobileNo id status level} createdAt assignedTo{_id mobileNo id status level} assignedAt acknowledgedAt resolvedAt}}`,
            variables: null
        }
        axios.post(`/graphql/ticket?`, input, getConfig())
            .then(response => {
                resolve(response.data.data.deleteTicket);
            }).catch(err => {
                //window.location.reload();
            })
    });
}
