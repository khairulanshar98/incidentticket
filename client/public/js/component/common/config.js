export var getConfig = () => {
    let config = {
        headers: {
            'Content-Type': 'application/json'
            , 'Authorization': 'Bearer ' + (localStorage.getItem('csrf') || "")
        }
    }
    return config;
}