import React from "react";
import two from '../../../assets/images/two.png';
import three from '../../../assets/images/three.png';
import four from '../../../assets/images/four.png';
import five from '../../../assets/images/five.png';
import { Row, Col } from 'react-bootstrap';
class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {

  }
  render() {
    return (
      <div style={{ color: "#4b4f56" }}>
        <div className="container" >
          <div className="jumbotron" style={{ marginTop: "20px" }}>
            <h1 style={{ marginBottom: "35px" }}>Incident Management</h1>
            <p>
              We are using <a href="https://nodejs.org/en/" target="_blank">Node.js</a> framework on the backend, and <a href="https://reactjs.org/" target="_blank">React</a> on the frontend.
          </p>
            <p>
              It is using a <a href="https://graphql.org" target="_blank">GraphQL</a> interface to handle the communication between client and server.
          </p>
            <p>
              The data is stored in <a href="https://www.mongodb.com/cloud/atlas" target="_blank">Mongo Atlas</a>.
          </p>
            <Row style={{ padding: "20px" }}>
              <Col sm={6}><img src={two} style={{ width: "100%" }} /></Col>
              <Col sm={6}><img src={three} style={{ width: "100%" }} /></Col>
            </Row>
            <Row style={{ padding: "20px" }}>
              <Col sm={6}><img src={four} style={{ width: "100%" }} /></Col>
              <Col sm={6}><img src={five} style={{ width: "100%" }} /></Col>
            </Row>
            <p>
              Source code: <a href="https://bitbucket.org/khairulanshar98/incidentticket/src/master/" target="_blank">incidentticket</a>.
          </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
