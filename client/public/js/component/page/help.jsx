import React, { Fragment } from "react";
import { Grid, Row, Col } from 'react-bootstrap/lib';
import one from '../../../assets/images/one.png';


class Help extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  componentDidMount() {
  }
  render() {
    return (
      <div style={{ color: "#4b4f56" }}>
        <div className="container" >
          <center>
            <h1>{this.props.title}</h1>
          </center>
          <div className="col-sm-10 col-sm-offset-1" style={{ marginTop: "20px" }}>
            <Row >
              <Col sm={6}>
                <dl>
                  <dt><h4>Login</h4></dt>
                  <dd>Please login using your mobile number</dd>
                  <dd>We will not store your mobile number</dd>
                  <dd>There are 2 roles, i.e. Admin and Normal User. You can set your role in the <a className="btn btn-link" href="#/user" style={{ padding: "0px" }}>User Page</a></dd>
                </dl>
                <dl>
                  <dt><h4>Features</h4></dt>
                  <dd>Raise an incident as an admin</dd>
                  <dd>Assign the incident to a user</dd>
                  <dd>Acknowledge the incident as a user</dd>
                  <dd>Resolve the incident as a user</dd>
                  <dd>Read details about a certain incident</dd>
                  <dd>Delete your own incident</dd>
                </dl>
                <p>
                  Please create .env file under server and client directory
                </p>
                <h4>.env for server</h4>
                <samp>PORT=</samp><br />
                <samp>DB_STR=</samp><br />
                <samp>DEFAULT_TIMEOUT=</samp><br />
                <samp>FILE_LIMIT=</samp><br />
                <samp>COOKIE_DOMAIN=</samp><br />
                <samp>COOKIE=</samp><br />
                <samp>APP_ID=</samp><br />
                <samp>APP_SEC=</samp><br />
                <samp>APP_KIT=</samp><br />
                <samp>SECRET=</samp><br />
                <samp>TOKEN_MAXAGE=</samp><br />
                <samp>NODE_ENV=</samp><br />
                <samp>TOKEN_SECURE=</samp><br />
                <samp>TOKEN_HTTPONLY=</samp><br />
                <h4>.env for client</h4>
                <samp>NODE_ENV=</samp>
                <dl>
                  <dt><h4>Contact Us</h4></dt>
                  <dd><a href="mailto:halo@khairulanshar.com" target="_blank">halo@khairulanshar.com</a></dd>
                </dl>
              </Col>
              <Col sm={6}><img src={one} style={{ width: "100%" }} /></Col>
            </Row>



          </div>
        </div>
      </div>
    );
  }
}

export default Help;
