import React from 'react';
import { Nav, Navbar, NavDropdown, NavItem, MenuItem } from 'react-bootstrap/lib';
import { onLogout } from '../common/login';
import AccountKit from 'react-facebook-account-kit';
import { loginAccountKit } from '../common/login';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appId: this.props.appId,
            version: this.props.version,
            csrf: this.props.csrf
        }
    };
    async componentDidMount() {

    }
    render() {
        var { user, isAuthenticated } = this.props
        return (
            <Navbar style={{ marginBottom: "0px", borderRadius: "0px" }}>
                <Navbar.Header className="hidden-xs hidden-sm">
                    <Navbar.Brand >
                        <a href="#/">Incident Management</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav className="pull-right">
                    <NavItem onClick={() => {
                        location.href = "#/";
                    }}>Home</NavItem>
                    {
                        isAuthenticated &&
                        <NavItem onClick={() => {
                            location.href = "#/ticket";
                        }}>Ticket</NavItem>
                    }
                    {
                        isAuthenticated &&
                        <NavItem onClick={() => {
                            location.href = "#/user";
                        }}>User</NavItem>
                    }
                    <NavItem onClick={() => {
                        location.href = "#/readme";
                    }}>Readme</NavItem>
                    {
                        isAuthenticated &&
                        <NavDropdown eventKey={3} title={user.mobileNo} id="basic-nav-dropdown">
                            <MenuItem eventKey={3.1} onClick={async () => {
                                var data = await onLogout();
                                this.setState({ csrf: data.kit.csrf, appId: data.kit.appId, version: data.kit.version })
                                this.props.logout();
                                location.href = "#/";
                            }}>Logout</MenuItem>
                            <MenuItem divider />
                            <MenuItem>v1.0.0</MenuItem>
                        </NavDropdown>
                    }
                    {
                        !isAuthenticated &&
                        <NavDropdown eventKey={4} title="Login" style={{ marginRight: "50px" }} id={"login"}>
                            {(this.props.appId) && <AccountKit
                                appId={this.state.appId}
                                version={this.state.version}
                                csrf={this.state.csrf}
                                onResponse={async (resp) => {
                                    var data = await loginAccountKit(resp);
                                    if (data.status == "OK" && data.user && data.user.id) {
                                        this.props.setLoginByPhone({ isAuthenticated: true, user: data.user })
                                    }
                                }}
                                countryCode={'+65'}
                            >
                                {p => <MenuItem eventKey={4.3} {...p}>Login</MenuItem>}
                            </AccountKit>}
                            <MenuItem divider />
                            <MenuItem>v1.0.0</MenuItem>
                        </NavDropdown>
                    }
                </Nav>
            </Navbar>
        )
    }
}

export default Header;
