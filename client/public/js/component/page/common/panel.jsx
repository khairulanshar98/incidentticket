import React from "react";
import { Row, Col } from 'react-bootstrap';
import { searchTicket, createTicket, assignTo, acknowledge, resolve, deleteTicket } from '../../common/ticket';
import { Modal, Button } from 'react-bootstrap';
import { timeSince } from '../../common/time';
import Autocomplete from 'react-autocomplete';
import { searchUser } from '../../common/user'
class Panel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            new: false,
            search: false,
            sort: "createdAt",
            descending: true,
            status: this.props.title,
            title: "",
            description: "",
            tickets: [],
            page: 1,
            showModal: false,
            ticket: {},
            autocompleteItem: [],
            mobileNo: "",
            assignTo: {},
            dirty: false,
            errmsg: ""
        }
        this.save = this.save.bind(this);
        this.search = this.search.bind(this);
    }
    async save() {
        if (this.state.title.trim() == '') {
            return this.setState({ errmsg: "Title is required" });
        }
        if (this.state.description.trim() == '') {
            return this.setState({ errmsg: "Description is required" });
        }
        await createTicket(this.state);
        this.setState({ title: "", description: "" });
        this.search();
    }
    async search() {
        var tickets = await searchTicket(this.state);
        this.setState({ search: false, dirty: false, showModal: false, new: false });
        this.props.setState_(`tickets${this.props.title}`, tickets)
    }
    componentDidMount() {
        this.search();
    }
    render() {
        const { title, type, createNew, user } = this.props
        return (
            <div className={`panel panel-${type}`}>
                {this.state.ticket.createdBy ? <Modal show={this.state.showModal}>
                    <Modal.Body>
                        <h3>{this.state.ticket.title}</h3>
                        <p dangerouslySetInnerHTML={{ __html: decodeURIComponent(this.state.ticket.description).replace(/\n/g, "<br/>") }}></p>

                        {this.state.ticket.status == "Unassigned" ? <div style={{ display: "block" }}>
                            <div className="form-group">
                                <label>Assign To</label>
                                <Autocomplete
                                    wrapperStyle={{ display: 'inline-block', width: '100%' }}
                                    getItemValue={(user) => {
                                        return user._id
                                    }}
                                    items={this.state.autocompleteItem}
                                    menuStyle={{
                                        zIndex: '999999999999',
                                        borderRadius: '3px',
                                        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                                        background: 'rgba(255, 255, 255, 0.9)',
                                        padding: '2px 0',
                                        fontSize: '14px',
                                        overflow: 'auto',
                                        position: 'absolute',
                                        left: 'auto',
                                        top: 'auto'
                                    }}
                                    renderInput={(props) => (
                                        <input
                                            {...props}
                                            type="text"
                                            className="form-control"
                                            placeholder="Search Mobile No"
                                        />
                                    )}
                                    renderItem={(item, isHighlighted) => (
                                        <div
                                            style={{
                                                background: isHighlighted ? 'lightgray' : 'white',
                                                padding: '5px 20px',
                                                cursor: 'pointer'
                                            }}
                                        >
                                            <a href="javascript:">{item.mobileNo}</a>
                                        </div>
                                    )}
                                    value={this.state.mobileNo}
                                    onChange={async (e) => {
                                        this.setState({ mobileNo: e.target.value });
                                        if (e.target.value.length > 0) {
                                            var filter = { mobileNo: e.target.value, level: "null", page: 1 }
                                            var users = await searchUser(filter);
                                            this.setState({ autocompleteItem: users })
                                        }
                                    }}
                                    onSelect={(_id) => {
                                        var index = this.state.autocompleteItem.findIndex(user => user._id == _id);
                                        var user = this.state.autocompleteItem[index];
                                        this.setState({ mobileNo: user.mobileNo, assignTo: user, dirty: true })
                                    }}
                                />

                            </div>
                        </div> : null}
                        <div style={{ display: "block", height: "18px" }}>
                            <span className={`label label-default pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(this.state.ticket.createdAt))}</span>
                            <span className={`label label-default pull-right`}><i className="glyphicon glyphicon-user" /> {this.state.ticket.createdBy.mobileNo}</span>
                        </div>
                        {this.state.ticket.assignedTo ?
                            <div style={{ display: "block", height: "18px" }}>
                                <span className={`label label-warning pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(this.state.ticket.assignedAt))}</span>
                                <span className={`label label-warning pull-right`}><i className="glyphicon glyphicon-user" /> {this.state.ticket.assignedTo.mobileNo}</span>
                            </div>
                            : null}
                        {this.state.ticket.acknowledgedAt ?
                            <div style={{ display: "block", height: "18px" }}>
                                <span className={`label label-success pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(this.state.ticket.acknowledgedAt))}</span>
                            </div>
                            : null}
                        {this.state.ticket.resolvedAt ?
                            <div style={{ display: "block", height: "18px" }}>
                                <span className={`label label-info pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(this.state.ticket.resolvedAt))}</span>
                            </div>
                            : null}
                    </Modal.Body>
                    <Modal.Footer>
                        {this.state.ticket.status != "Resolved" ? <Button
                            className={`btn btn-primary btn-sm`}
                            onClick={async () => {
                                if (this.state.ticket.status == "Unassigned") {
                                    await assignTo(this.state.ticket._id, this.state.assignTo._id);
                                    var tickets = await searchTicket({ title: "", description: "", status: "Assigned", sort: "assignedAt", descending: true, page: 1 });
                                    this.props.setState_(`ticketsAssigned`, tickets)
                                }
                                if (this.state.ticket.status == "Assigned") {
                                    await acknowledge(this.state.ticket._id);
                                    var tickets = await searchTicket({ title: "", description: "", status: "Acknowledged", sort: "acknowledgedAt", descending: true, page: 1 });
                                    this.props.setState_(`ticketsAcknowledged`, tickets)
                                }
                                if (this.state.ticket.status == "Acknowledged") {
                                    await resolve(this.state.ticket._id);
                                    var tickets = await searchTicket({ title: "", description: "", status: "Resolved", sort: "ResolvedAt", descending: true, page: 1 });
                                    this.props.setState_(`ticketsResolved`, tickets)
                                }
                                await this.search();
                            }}
                            disabled={(this.state.ticket.status == "Unassigned" && !this.state.dirty) || ((this.state.ticket.status == "Acknowledged" || this.state.ticket.status == "Assigned") && this.state.ticket.assignedTo._id != this.props.user._id)}
                            style={{ marginRight: "5px" }}>
                            {this.state.ticket.status == "Unassigned" ? "Save" : null}
                            {this.state.ticket.status == "Assigned" ? "Acknowledge" : null}
                            {this.state.ticket.status == "Acknowledged" ? "Resolve" : null}
                        </Button> : null}
                        <Button
                            className={`btn btn-danger btn-sm`}
                            onClick={() => {
                                var r = false;
                                if (this.state.dirty) {
                                    var r = confirm("Discard your changes?");
                                }
                                if (r || !this.state.dirty)
                                    this.setState({ ticket: {}, showModal: false, mobileNo: "", assignTo: {} })
                            }}>
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal> : null}
                <div className="panel-heading">
                    <Button className={`btn btn-${type} btn-xs pull-right`}
                        onClick={async () => {
                            this.state.title = "";
                            this.state.description = "";
                            var tickets = await searchTicket(this.state);
                            this.setState({ title: "", description: "", search: false, dirty: false, showModal: false });
                            this.props.setState_(`tickets${this.props.title}`, tickets)
                        }}
                    ><span className="glyphicon glyphicon-refresh" /></Button>
                    <h3 className="panel-title">{title}</h3>
                </div>
                <div className="panel-body">
                    {(this.state.new) ?
                        <Row style={{ marginTop: "10px" }}>
                            <Col className="col-sm-10 col-sm-offset-1">
                                <div className="form-group">
                                    <label>Title</label>
                                    <input type="text" className="form-control" placeholder="Title"
                                        value={this.state.title}
                                        onChange={(e) => { this.setState({ title: e.target.value, errmsg: "" }) }} />
                                </div>
                                <div className="form-group">
                                    <label>Description</label>
                                    <textarea rows="4" className="form-control" placeholder="Description"
                                        value={this.state.description}
                                        onChange={(e) => { this.setState({ description: e.target.value, errmsg: '' }) }} />
                                </div>
                            </Col>
                            {(this.state.errmsg.length) ?
                                <Col className="col-sm-10 col-sm-offset-1 text-center">
                                    <div class="alert alert-warning" role="alert">{this.state.errmsg}</div>
                                </Col>
                                : null}
                            <Col className="col-sm-10 col-sm-offset-1 text-center">
                                <button type="button" className="btn btn-danger btn-sm" style={{ marginRight: "5px" }}
                                    onClick={() => { this.setState({ new: false }) }}>Cancel</button>
                                <button type="button" className="btn btn-primary btn-sm" onClick={this.save}>Save</button>
                            </Col>
                        </Row> : null}

                    {(this.state.search) ?
                        <Row style={{ marginTop: "10px" }}>
                            <Col className="col-sm-10 col-sm-offset-1">
                                <div className="form-group">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" className="form-control" placeholder="Title"
                                        value={this.state.title}
                                        onChange={(e) => { this.setState({ title: e.target.value }) }} />
                                </div>
                                <div className="form-group">
                                    <label for="exampleInputEmail1">Description</label>
                                    <textarea rows="4" className="form-control" placeholder="Description"
                                        value={this.state.description}
                                        onChange={(e) => { this.setState({ description: e.target.value }) }} />
                                </div>
                                <div className="form-group">
                                    <label for="exampleInputEmail1">Sort By</label>
                                    <select className="form-control" id="sort" name="sort" ref="sort" value={this.state.sort}
                                        onChange={(e) => {
                                            this.setState({ sort: e.target.value });
                                        }}>
                                        <option value={"null"}></option>
                                        <option value={"title"}>Title</option>
                                        <option value={"description"}>Description</option>
                                        <option value={"status"}>Status</option>
                                        <option value={"createdAt"}>Created at</option>
                                        <option value={"assignedAt"}>Assigned at</option>
                                        <option value={"acknowledgedAt"}>Acknowledged at</option>
                                        <option value={"resolvedAt"}>Resolved at</option>
                                    </select>
                                </div>
                                <div className="checkbox">
                                    <label>
                                        <input type="checkbox" checked={this.state.descending}
                                            onChange={(e) => {
                                                this.setState({ descending: e.target.checked });
                                            }} /> Descending
                                    </label>
                                </div>
                            </Col>
                            <Col className="col-sm-10 col-sm-offset-1 text-center">
                                <button type="button" className="btn btn-danger btn-sm" style={{ marginRight: "5px" }}
                                    onClick={() => { this.setState({ search: false }) }}>Cancel</button>
                                <button type="button" className="btn btn-primary btn-sm"
                                    onClick={this.search}>Search</button>
                            </Col>
                        </Row> : null}

                    {(!this.state.search && !this.state.new) ?
                        <Row style={{ marginTop: "5px", padding: "5px 15px" }}>
                            {this.props.tickets &&
                                this.props.tickets.map((ticket, i) => {
                                    return (
                                        <div className={`panel panel-${type} ticket-panel`}>
                                            <div className="panel-heading">
                                                <Button className={`btn btn-danger btn-xs pull-right`}
                                                    onClick={async () => {
                                                        await deleteTicket(ticket._id);
                                                        await this.search();
                                                    }}
                                                    disabled={(ticket.assignedTo && ticket.assignedTo._id != this.props.user._id) && (ticket.createdBy && ticket.createdBy._id != this.props.user._id)}
                                                ><span className="glyphicon glyphicon-trash" /></Button>
                                                <div onClick={() => { this.setState({ showModal: true, ticket: ticket }) }}>
                                                    <h3 className="panel-title">{ticket.title}</h3>
                                                    <p>{decodeURIComponent(ticket.description)}</p>
                                                    <div style={{ display: "block", height: "18px" }}>
                                                        <span className={`label label-default pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(ticket.createdAt))}</span>
                                                        <span className={`label label-default pull-right`}><i className="glyphicon glyphicon-user" /> {ticket.createdBy.mobileNo}</span>
                                                    </div>

                                                    {ticket.assignedTo ?
                                                        <div style={{ display: "block", height: "18px" }}>
                                                            <span className={`label label-warning pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(ticket.assignedAt))}</span>
                                                            <span className={`label label-warning pull-right`}><i className="glyphicon glyphicon-user" /> {ticket.assignedTo.mobileNo}</span>
                                                        </div>
                                                        : null}
                                                    {ticket.acknowledgedAt ?
                                                        <div style={{ display: "block", height: "18px" }}>
                                                            <span className={`label label-success pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(ticket.acknowledgedAt))}</span>
                                                        </div>
                                                        : null}
                                                    {ticket.resolvedAt ?
                                                        <div style={{ display: "block", height: "18px" }}>
                                                            <span className={`label label-info pull-left`}><i className="glyphicon glyphicon-time" /> {timeSince(parseInt(ticket.resolvedAt))}</span>
                                                        </div>
                                                        : null}
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                        </Row>
                        : null}
                </div>
                <div className="panel-footer">
                    {createNew && <button type="button" className={`btn btn-${type} btn-sm`}
                        onClick={() => { this.setState({ new: true, search: false, title: "", description: "" }) }}
                        style={{ marginRight: "5px" }} disabled={user && user.level == 2}><span className="glyphicon glyphicon-plus" /> Create new ticket</button>}
                    <button type="button" className={`btn btn-${type} btn-sm`}
                        onClick={() => { this.setState({ new: false, search: true, title: "", description: "" }) }}
                    ><span className="glyphicon glyphicon-search" /> Search</button>

                </div>
            </div>
        );
    }
}

export default Panel;
