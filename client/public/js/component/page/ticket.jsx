import React from "react";
import { Row, Col } from 'react-bootstrap';
import Panel from './common/panel.jsx';

class Ticket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ticketsUnassigned: [],
      ticketsAssigned: [],
      ticketsAcknowledged: [],
      ticketsResolved: []
    }
    this.setState_ = this.setState_.bind(this);
  }
  setState_(item, data) {
    var s = {};
    s[item] = data;
    this.setState(s);
  }
  componentDidMount() {
    const { isAuthenticated } = this.props;
    if (!isAuthenticated) location.href = "#/";
  }
  render() {
    const { isAuthenticated } = this.props;
    if (!isAuthenticated) return null;
    return (
      <div style={{ color: "#4b4f56" }}>
        <div className="container ticket" >
          <center>
            <h1>{this.props.title}</h1>
          </center>
          <Row style={{ marginTop: "50px" }}>
            <Col className="col-sm-3">
              <Panel title="Unassigned" type="default" setState_={this.setState_} tickets={this.state.ticketsUnassigned} createNew={true} user={this.props.user}></Panel>
            </Col>
            <Col className="col-sm-3">
              <Panel title="Assigned" setState_={this.setState_} tickets={this.state.ticketsAssigned} type="warning" createNew={false} user={this.props.user}></Panel>
            </Col>
            <Col className="col-sm-3">
              <Panel title="Acknowledged" setState_={this.setState_} tickets={this.state.ticketsAcknowledged} type="success" createNew={false} user={this.props.user}></Panel>
            </Col>
            <Col className="col-sm-3">
              <Panel title="Resolved" setState_={this.setState_} tickets={this.state.ticketsResolved} type="info" createNew={false} user={this.props.user}></Panel>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Ticket;
