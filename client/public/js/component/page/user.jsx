import React from "react";
import { Grid, Row, Col } from 'react-bootstrap';
import { searchUser, promoteTo1, promoteTo2 } from '../common/user'


class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      mobileNo: "",
      level: "null",
      page: 1,
      isLoading: false,
      disablenext: true,
      suara: [],
      showModal: false,
      isLoadingModal: false
    }
    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  handleClose() {
    this.setState({ showModal: false });
  }

  componentDidMount() {
    const { isAuthenticated } = this.props;
    if (!isAuthenticated) location.href = "#/";
  }
  async promoteTo1(_id) {
    var user = await promoteTo1(_id);
    var index = this.state.users.findIndex(user => user._id == _id);
    var tmp = Object.assign({}, this.state);
    tmp.users[index] = user;
    this.setState(tmp);
  }
  async promoteTo2(_id) {
    var user = await promoteTo2(_id);
    var index = this.state.users.findIndex(user => user._id == _id);
    var tmp = Object.assign({}, this.state);
    tmp.users[index] = user;
    this.setState(tmp);
  }

  async search() {
    this.setState({ isLoading: true })
    var filter = { mobileNo: "null", level: "null", page: this.state.page }
    if (this.state.mobileNo && this.state.mobileNo.trim().length) {
      filter.mobileNo = this.state.mobileNo.trim()
    }
    filter.level = this.state.level;
    var users = await searchUser(filter);
    this.setState({ users: users, isLoading: false, disablenext: (users.length < 50) })
  }

  prev(e) {
    if (this.state.page <= 1) {
      return;
    }
    var page = this.state.page - 1;
    this.setState({ page: page })
    var t = this;
    setTimeout(() => {
      t.search();
    }, 1000)
  }
  next(e) {
    var page = this.state.page + 1;
    this.setState({ page: page })
    var t = this;
    setTimeout(() => {
      t.search();
    }, 1000)
  }



  render() {
    const { isAuthenticated } = this.props;
    if (!isAuthenticated) return null;
    return (
      <div style={{ color: "#4b4f56" }}>
        <div className="container" >
          <Row style={{ marginTop: "50px" }}>
            <Col className="col-sm-6 col-sm-offset-3">
              <Row style={{ marginBottom: "15px" }}>
                <Col sm={3} className="text-right"><b>Mobile No</b></Col>
                <Col sm={9} className="text-left">
                  <input
                    onChange={(e) => this.setState({ mobileNo: e.target.value })}
                    type="text"
                    className="form-control"
                    value={this.state.mobileNo}
                  />
                </Col>
              </Row>
              <Row style={{ marginBottom: "15px" }}>
                <Col sm={3} className="text-right"><b>Level</b></Col>
                <Col sm={9} className="text-left">
                  <select className="form-control" id="level" name="level" ref="level" value={this.state.level}
                    onChange={(e) => {
                      this.setState({ level: e.target.value });
                    }}>
                    <option value={"null"}></option>
                    <option value={"1"}>Admin</option>
                    <option value={"2"}>User</option>
                  </select>
                </Col>
              </Row>
              <Row style={{ marginBottom: "15px" }}>
                <Col sm={3} className="text-right"></Col>
                <Col sm={9} className="text-left">
                  <button className="btn btn-primary" onClick={() => {
                    this.search();
                  }}>Search</button>
                </Col>
              </Row>
            </Col>
          </Row>

          {this.state.users && <Grid style={{ marginTop: "50px" }}>
            <Row >
              <nav>
                <ul className="pager pull-right">
                  <li className="previous"><a href="javascript:"><span aria-hidden="true" onClick={this.prev} disabled={(parseInt(this.state.page) <= 1)}>&larr;</span> Older</a></li>
                  <li className="next"><a href="javascript:">Newer <span aria-hidden="true" onClick={this.next} disabled={this.state.disablenext}>&rarr;</span></a></li>
                </ul>
              </nav>
            </Row>
            <center>
              <table className="table table-striped table-hover" style={{ fontWeight: "300" }}>
                <thead>
                  <tr>
                    <th className="text-left">Mobile No</th>
                    <th className="text-left">Level</th>
                    <th className="text-left">Status</th>
                    <th className="text-left">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    !this.state.isLoading && this.state.users && this.state.users.map((result, i) => {
                      if (result._id == "0") return null;
                      return (
                        <tr key={i} style={{ fontWeight: "400" }}>
                          <td className="text-left">{result.mobileNo}</td>
                          <td className="text-left">{(result.level == 2) ? "User" : "Admin"}</td>
                          <td className="text-left">{(result.status) ? "Enabled" : "Disabled"}</td>
                          <td className="text-left" style={{ width: "70px" }}>
                            <button className="btn btn-sm btn-danger" onClick={() => {
                              this.promoteTo1(result._id);
                            }} style={{ marginTop: "3px" }} disabled={(result.level == 1)}>Set as Admin</button>
                            <button className="btn btn-sm btn-info" onClick={() => {
                              this.promoteTo2(result._id);
                            }} style={{ marginTop: "3px" }} disabled={(result.level == 2)}>Set as User</button>
                          </td>
                        </tr>
                      )
                    })}
                </tbody>
              </table>
            </center>
            <Row >
              <nav>
                <ul className="pager pull-right">
                  <li className="previous"><a href="javascript:"><span aria-hidden="true" onClick={this.prev} disabled={(parseInt(this.state.page) <= 1)}>&larr;</span> Older</a></li>
                  <li className="next"><a href="javascript:">Newer <span aria-hidden="true" onClick={this.next} disabled={this.state.disablenext}>&rarr;</span></a></li>
                </ul>
              </nav>
            </Row>
          </Grid>
          }
        </div>
      </div>
    );
  }
}

export default User;
