import React, { Component, PropTypes } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Header from './component/page/header.jsx';
import Home from './component/page/home.jsx';
import Help from './component/page/help.jsx';
import User from './component/page/user.jsx';
import Ticket from './component/page/ticket.jsx';
import { checkToken, getAccountKit, graphToken } from './component/common/login';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: false,
            user: null,
            csrf: null
        }
        this.setLoginByPhone = this.setLoginByPhone.bind(this);
    }
    logout() {
        this.setState({ isAuthenticated: false, user: null });
        location.reload();
    }

    async componentDidMount() {
        var t = this;
        var data = await graphToken();
        if (data.user && data.user._id) {
            t.setState({ isAuthenticated: true, user: data.user, csrf: data.csrf });
        } else {
            var tmp = Object.assign(this.state, data.kit);
            this.setState(tmp);
        }
    }
    setLoginByPhone(data) {
        this.setState(data);
    }
    render() {
        if (!this.state.csrf) return <span></span>;
        return (
            <Router>
                <div>
                    <Header csrf={this.state.csrf} appId={this.state.appId} version={this.state.version} setLoginByPhone={this.setLoginByPhone} store={this.props} logout={() => { this.logout() }} isAuthenticated={this.state.isAuthenticated} user={this.state.user} />
                    <Switch>
                        <Route exact path={'/' || '/_=_'} render={(route) => (
                            <Home title="Home" route={route} result={this.state.result} provs={this.state.provs} isAuthenticated={this.state.isAuthenticated} user={this.state.user}/>
                        )} />
                        <Route exact path='/ticket' render={(route) => (
                            <Ticket title="Ticket" route={route} result={this.state.result} provs={this.state.provs} isAuthenticated={this.state.isAuthenticated} user={this.state.user}/>
                        )} />
                        <Route exact path='/readme' render={(route) => (
                            <Help title="Readme" route={route} result={this.state.result} provs={this.state.provs} isAuthenticated={this.state.isAuthenticated} user={this.state.user}/>
                        )} />
                        <Route exact path='/user' render={(route) => (
                            <User title="User" route={route} isAuthenticated={this.state.isAuthenticated} user={this.state.user}/>
                        )} />
                    </Switch>
                </div>
            </Router>
        );
    }
}


export default (App);
