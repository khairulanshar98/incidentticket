const User = require('../models/User');
const jws = require('jws');
const jsonwebtoken = require('jsonwebtoken');
var env = require('node-env-file');
env('.env');
var Accountkit = require('node-accountkit');
Accountkit.set(process.env.APP_ID, process.env.APP_KIT, "v1.1"); //API_VERSION is optional, default = v1.1
Accountkit.requireAppSecret(true);
var conf = require('../config');
var uuidv4 = require('uuid/v4');

const getPhone = (token) => {
    return new Promise((resolve, reject) => {
        Accountkit.getAccountInfo(token, function (err, res) {
            if (err) return resolve({ error: err, data: null });
            resolve({ error: null, data: res });
        });
    });
}
const phoneVerify = (token) => {
    return new Promise(async (resolve, reject) => {
        try {
            var user = await getPhone(token);
            if (user.data) {
                user = user.data;
                user.mobileNo = user.phone.number;
                user.id = "18." + user.id;
                user.type = 'phone';
                return resolve({ error: null, data: user });
            }
            if (user.error) {
                resolve({ error: user.error, data: null });
            }
        } catch (e) {
            return resolve({ error: e, data: null });
        }
    });
}

const getRawCookies = (req, attribute) => {
    if (req.headers && req.headers["cookie"]) {
        var cookies = req.headers["cookie"].split("; ");
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i];
            let kv = cookie.split("=");
            if (kv[0] === attribute)
                return kv[1];
        }
    }
    return null;
};

const getAuthorization = (req) => {
    if (req.headers.authorization) {
        var data = req.headers.authorization.split(" ");
        if (data.length == 1) return data[0];
        if (data.length == 2) return data[1];
        return "";
    }
    else return "";
};

const decodeToken = (rawToken) => {
    if (rawToken) {
        return jws.decode(rawToken);
    }
    return null;
}

const getToken = (req, attribute) => {
    let rawToken = getRawCookies(req, attribute);
    return decodeToken(rawToken)
}

var searchUser = (q, p, l) => {
    return new Promise((resolve, reject) => {
        User.find(q)
            .limit(l)
            .skip((p - 1) * l)
            .populate([
                {
                    path: "promotedBy",
                    model: "User",
                    select: "_id id mobileNo status level"
                }
            ])
            .sort({ mobileNo: 1 })
            .exec(function (err, data) {
                if (err) return resolve([]);
                resolve(data);
            });
    });
};

const getUserById = (id) => {
    return new Promise((resolve, reject) => {
        User.findOne({ id: id })
            .exec((err, doc) => {
                if (err)
                    return resolve({ error: err, data: null });
                else
                    resolve({ error: null, data: doc });
            });
    });
}

const getUserBy_Id = (id) => {
    return new Promise((resolve, reject) => {
        User.findById(id)
            .populate([
                {
                    path: "promotedBy",
                    model: "User",
                    select: "_id id mobileNo status level"
                }
            ])
            .exec((err, doc) => {
                if (err) return resolve(null);
                resolve(doc);
            });
    });
}

var createUser = (data) => {
    return new Promise((resolve, reject) => {
        User.create(data, (err, doc) => {
            if (err) return resolve({ error: err, data: null });
            doc = doc.toObject();
            resolve({ error: null, data: doc });
        });
    });
}
var updateUser = (doc, data) => {
    return new Promise((resolve, reject) => {
        doc = Object.assign(doc, data);
        doc.save((err1, doc1) => {
            if (err1) return resolve(null);
            resolve(doc1);
        })
    });
}

const isTokenValid = (req, attribute) => {
    try {
        var decoded = jsonwebtoken.verify(getRawCookies(req, attribute), process.env.SECRET);
        return decoded != null
    } catch (err) {
        return false
    }
}
const createToken = (data) => {
    return jsonwebtoken.sign(data, process.env.SECRET, { expiresIn: process.env.TOKEN_MAXAGE });
}
const cookieObject = (req) => {
    return {
        secure: /true/i.test(process.env.TOKEN_SECURE),
        httpOnly: /true/i.test(process.env.TOKEN_HTTPONLY),
        path: "/",
        domain: process.env.COOKIE_DOMAIN,
        maxAge: process.env.TOKEN_MAXAGE
    }
}

const resetCookie = (req, res) => {
    var obj = conf.phone;
    delete obj.clientSecret;
    obj.csrf = uuidv4().replace(/-/g, '');
    obj.version = "v1.1";
    const newToken = createToken(obj);
    res.cookie(process.env.COOKIE, newToken, cookieObject(req));
    res.setHeader('csrf', obj.csrf);
    return obj;
}

module.exports.resetCookie = resetCookie;
module.exports.phoneVerify = phoneVerify;
module.exports.getRawCookies = getRawCookies;
module.exports.getAuthorization = getAuthorization;
module.exports.createToken = createToken;
module.exports.isTokenValid = isTokenValid;
module.exports.getToken = getToken;
module.exports.getUserById = getUserById;
module.exports.getUserBy_Id = getUserBy_Id;
module.exports.createUser = createUser;
module.exports.updateUser = updateUser;
module.exports.searchUser = searchUser;
module.exports.cookieObject = cookieObject;