const { makeExecutableSchema } = require('graphql-tools');
const env = require('node-env-file');
env('.env');
var { getUserBy_Id, createToken, getToken, isTokenValid, cookieObject, resetCookie } = require('../lib/admin');
var uuidv4 = require('uuid/v4');
const resolverObject = {
    Query: {
        async getToken(root, { }, ctx) {
            if (isTokenValid(ctx, process.env.COOKIE)) {
                let token = getToken(ctx, process.env.COOKIE);
                let obj = token.payload;
                var user = await getUserBy_Id(obj._id);
                if (user) {
                    obj = user.toObject();
                    obj.csrf = uuidv4().replace(/-/g, '');
                    const newToken = createToken(obj);
                    ctx.res.cookie(process.env.COOKIE, newToken, cookieObject(ctx));
                    ctx.res.setHeader('csrf', obj.csrf);
                    return { status: 'OK', msg: "success", kit: null, user: obj, csrf: obj.csrf };
                }
            }
            var obj = resetCookie(ctx, ctx.res)
            return { status: 'NOT OK', msg: "", kit: obj, csrf: obj.csrf };
        },
        clearToken(root, { }, ctx) {
            var obj = resetCookie(ctx, ctx.res)
            return { status: 'NOT OK', msg: "", kit: obj, csrf: obj.csrf };
        }
    }
};


const typeDefinition = `
type User {
    _id: ID!
    id: String!
    mobileNo: String!
    status: Int!
    level: Int!
    promotedBy: User
    csrf: String
}

type Kit {
    appId: String!
    version: String!
    csrf: String
}

type cookie {
  csrf: String
  status: String
  user: User
  msg: String
  kit: Kit
 }

 type Query {
    getToken: cookie
    clearToken: cookie
 }

`;
var TokenSchema = makeExecutableSchema({ typeDefs: typeDefinition, resolvers: resolverObject })
module.exports = TokenSchema;