const { makeExecutableSchema } = require('graphql-tools');
const { getToken, searchUser, getUserBy_Id } = require('../lib/admin');
const User = require('../models/User');
const resolverObject = {
    Query: {
        async getUsers(root, { mobileNo, level, page }, ctx) {
            var q = {}
            if (mobileNo != 'null') {
                q.mobileNo = { $regex: new RegExp(`.*${mobileNo}.*`, 'i') };
            }
            if (level != 'null') {
                q.level = parseInt(level);
            }
            var data = await searchUser(q, page, 50);
            return data;
        }
    },
    Mutation: {
        async createUser(root, { input }) {
            return await User.create(input);
        },
        async promoteTo1(root, { _id }, ctx) {
            var targetUser = await getUserBy_Id(_id);
            if (!targetUser) return null;
            var req = ctx;
            let token = getToken(req, process.env.COOKIE);
            let obj = token.payload;
            let user = await getUserBy_Id(obj._id);
            if (!user) return null;
            //if (user && user.canPromoteTo1() && targetUser.canBePromotedTo1()) {
            if (user && targetUser.canBePromotedTo1()) {
                Object.assign(targetUser, {
                    level: 1,
                    status: 1,
                    promotedBy: user._id,
                });
                await targetUser.save();
                targetUser = await getUserBy_Id(_id);
                return targetUser;
            }
            return targetUser;
        },
        async promoteTo2(root, { _id }, ctx) {
            var targetUser = await getUserBy_Id(_id);
            if (!targetUser) return null;
            var req = ctx;
            let token = getToken(req, process.env.COOKIE);
            let obj = token.payload;
            let user = await getUserBy_Id(obj._id);
            if (!user) return null;
            Object.assign(targetUser, {
                level: 2,
                status: 1,
                promotedBy: user._id,
            });
            await targetUser.save();
            targetUser = await getUserBy_Id(_id);
            return targetUser;
        },
    }
};


const typeDefinition = `

type User {
  _id: ID!
  id: String!
  mobileNo: String!
  status: Int!
  level: Int!
  promotedBy: User
 }

 type Query {
    getUsers(mobileNo: String, level: String, page: Int): [User]
 }

 input UserInput {
    id: String
    mobileNo: String
 }

  type Mutation {
    createUser(input: UserInput) : User
    promoteTo1(_id: ID): User
    promoteTo2(_id: ID): User
   }

`;
var UserSchema = makeExecutableSchema({ typeDefs: typeDefinition, resolvers: resolverObject })

module.exports = UserSchema;