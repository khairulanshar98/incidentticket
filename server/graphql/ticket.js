const { makeExecutableSchema } = require('graphql-tools');
const { getToken, searchUser, getUserBy_Id } = require('../lib/admin');
const Ticket = require('../models/Ticket');

var searchTicket = (q, s, p, l) => {
    return new Promise((resolve, reject) => {
        Ticket.find(q)
            .limit(l)
            .skip((p - 1) * l)
            .populate([
                {
                    path: "createdBy",
                    model: "User",
                    select: "_id id mobileNo status level"
                },
                {
                    path: "assignedTo",
                    model: "User",
                    select: "_id id mobileNo status level"
                }
            ])
            .sort(s)
            .exec(function (err, data) {
                if (err) return resolve([]);
                resolve(data);
            });
    });
};
const resolverObject = {
    Query: {
        async getTickets(root, { title, description, status, sort, descending, page }, ctx) {
            var q = {}
            if (title != '') {
                q.title = { $regex: new RegExp(`.*${title}.*`, 'i') };
            }
            if (description != '') {
                q.description = { $regex: new RegExp(`.*${description}.*`, 'i') };
            }
            if (status != '') {
                q.status = status;
            }
            var s = {}
            s[sort] = descending;
            var data = await searchTicket(q, s, page, 50);
            return data;
        },
        async getTicketById(root, {
            _id
        }) {
            var data = await Ticket.findById(_id)
                .populate([
                    {
                        path: "createdBy",
                        model: "User",
                        select: "_id id mobileNo status level"
                    },
                    {
                        path: "assignedTo",
                        model: "User",
                        select: "_id id mobileNo status level"
                    }
                ]);
            return data;
        }
    },
    Mutation: {
        async createTicket(root, { input }, ctx) {
            var req = ctx;
            let token = getToken(req, process.env.COOKIE);
            let obj = token.payload;
            let user = await getUserBy_Id(obj._id);
            if (!user || !user.isAdmin()) return null;
            else {
                input.createdBy = user;
                var data = await Ticket.create(input);
                data = await Ticket.findById(data._id)
                    .populate([
                        {
                            path: "createdBy",
                            model: "User",
                            select: "_id id mobileNo status level"
                        },
                        {
                            path: "assignedTo",
                            model: "User",
                            select: "_id id mobileNo status level"
                        }
                    ]);
                return data;
            }
        },
        async assignTo(root, { _id, user_id }, ctx) {
            var ticket = await Ticket.findById(_id);
            if (!ticket) return null;
            var req = ctx;
            let token = getToken(req, process.env.COOKIE);
            let obj = token.payload;
            let user = await getUserBy_Id(obj._id);
            if (!user) return ticket;
            if (ticket.status == "Unassigned") {
                Object.assign(ticket, {
                    status: "Assigned",
                    assignedTo: user_id,
                    assignedAt: new Date()
                });
                await ticket.save();
            }
            var data = await Ticket.findById(_id)
                .populate([
                    {
                        path: "createdBy",
                        model: "User",
                        select: "_id id mobileNo status level"
                    },
                    {
                        path: "assignedTo",
                        model: "User",
                        select: "_id id mobileNo status level"
                    }
                ]);
            return data;
        },
        async acknowledge(root, { _id }, ctx) {
            var ticket = await Ticket.findById(_id);
            if (!ticket) return null;
            var req = ctx;
            let token = getToken(req, process.env.COOKIE);
            let obj = token.payload;
            let user = await getUserBy_Id(obj._id);
            if (!user) return null;
            if (user._id.toString() == ticket.assignedTo.toString() && ticket.status == "Assigned") {
                Object.assign(ticket, {
                    status: "Acknowledged",
                    acknowledgedAt: new Date()
                });
                await ticket.save();
            }
            var data = await Ticket.findById(_id)
                .populate([
                    {
                        path: "createdBy",
                        model: "User",
                        select: "_id id mobileNo status level"
                    },
                    {
                        path: "assignedTo",
                        model: "User",
                        select: "_id id mobileNo status level"
                    }
                ]);
            return data;
        },
        async resolve(root, { _id }, ctx) {
            var ticket = await Ticket.findById(_id);
            if (!ticket) return null;
            var req = ctx;
            let token = getToken(req, process.env.COOKIE);
            let obj = token.payload;
            let user = await getUserBy_Id(obj._id);
            if (!user) return null;
            if (user._id.toString() == ticket.assignedTo.toString() && ticket.status == "Acknowledged") {
                Object.assign(ticket, {
                    status: "Resolved",
                    resolvedAt: new Date()
                });
                await ticket.save();
            }
            var data = await Ticket.findById(_id)
                .populate([
                    {
                        path: "createdBy",
                        model: "User",
                        select: "_id id mobileNo status level"
                    },
                    {
                        path: "assignedTo",
                        model: "User",
                        select: "_id id mobileNo status level"
                    }
                ]);
            return data;
        },
        async deleteTicket(root, { _id }, ctx) {
            var ticket = await Ticket.findById(_id);
            if (!ticket) return null;
            var req = ctx;
            let token = getToken(req, process.env.COOKIE);
            let obj = token.payload;
            let user = await getUserBy_Id(obj._id);
            if (!user) return null;
            if ((ticket.assignedTo && user._id.toString() == ticket.assignedTo.toString()) ||
                user._id.toString() == ticket.createdBy.toString()) {
                Object.assign(ticket, {
                    status: "Deleted",
                });
                await ticket.save();
            }
            var data = await Ticket.findById(_id)
                .populate([
                    {
                        path: "createdBy",
                        model: "User",
                        select: "_id id mobileNo status level"
                    },
                    {
                        path: "assignedTo",
                        model: "User",
                        select: "_id id mobileNo status level"
                    }
                ]);
            return data;
        },
    }
};


const typeDefinition = `
type User {
    _id: ID!
    id: String
    mobileNo: String
    status: Int
    level: Int
   }

type Ticket {
  _id: ID!
  title: String!
  description: String!
  status: String!
  createdBy: User!
  createdAt: String
  assignedTo: User
  assignedAt: String
  acknowledgedAt: String
  resolvedAt: String
 }

 type Query {
    getTickets(title: String, description: String, sort:String, descending:Int, status: String, descending: Int, page: Int): [Ticket]
    getTicketById(_id: ID): User
 }

 input TicketInput {
    title: String
    description: String
 }

  type Mutation {
    createTicket(input: TicketInput) : Ticket
    assignTo(_id: ID, user_id: String): Ticket
    acknowledge(_id: ID): Ticket
    resolve(_id: ID): Ticket
    deleteTicket(_id: ID): Ticket
   }

`;
var TicketSchema = makeExecutableSchema({ typeDefs: typeDefinition, resolvers: resolverObject })

module.exports = TicketSchema;