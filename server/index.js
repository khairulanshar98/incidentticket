var express = require('express');
var app = express();
const cors = require('cors');
const env = require('node-env-file');
env('.env');
var { isTokenValid, getToken, getAuthorization, getUserBy_Id, cookieObject, createToken, resetCookie } = require('./lib/admin.js');
const path = require('path');
const graphlHTTP = require('express-graphql');
const UserSchema = require('./graphql/user');
const TokenSchema = require('./graphql/token');
const TicketSchema = require('./graphql/ticket');

// load mongoose package
var mongoose = require('mongoose');
// Use native Node promises
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_STR, { useNewUrlParser: true })
    .then((mongoose) => {
        console.log('connection succesful');
    })
    .catch((err) => console.error(err));

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
app.use(bodyParser.json({
    limit: process.env.FILE_LIMIT || '100mb'
})), cors();
app.use(bodyParser.urlencoded({
    limit: process.env.FILE_LIMIT || '100mb',
}), cors());
app.use(cookieParser({
    limit: process.env.FILE_LIMIT || '100mb'
}), cors());

const nocache = (req, res, next) => {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
}
const nohttp = (req, res, next) => {
    res.header('X-Frame-Options', 'SAMEORIGIN');
    res.header('Strict-Transport-Security', 'max-age=31536000');
    next();
}
app.use(express.static(path.join(__dirname, 'public')), nohttp);

var isValidUser = async (req, res, next) => {
    if (isTokenValid(req, process.env.COOKIE)) {
        let token = getToken(req, process.env.COOKIE);
        let obj = token.payload;
        var user = await getUserBy_Id(obj._id);
        if (user) {
            if (user.canEntryData())
                return next();
        }
    }
    res.clearCookie(process.env.COOKIE);
    var obj = resetCookie(req, res);
    return res.status(403).send({
        status: 'ERROR',
        message: "insufficient permission."
    });
}
var isCsrfValid = async (req, res, next) => {
    if (isTokenValid(req, process.env.COOKIE)) {
        let token = getToken(req, process.env.COOKIE);
        let obj = token.payload;
        var csrf = getAuthorization(req)
        if (obj.csrf == csrf) {
            delete obj.exp;
            delete obj.iat;
            const newToken = createToken(obj);
            res.cookie(process.env.COOKIE, newToken, cookieObject(req));
            return next();
        }
    }
    res.clearCookie(process.env.COOKIE);
    var obj = resetCookie(req, res);
    return res.status(403).send({
        status: 'ERROR',
        message: "insufficient permission."
    });
}

function cache(req, res, next) {
    res.header('Pragma', 'public');
    res.header('Cache-Control', 'max-age=864000000');
    next()
}


app.use('/graphql/token', nohttp, nocache, graphlHTTP({
    schema: TokenSchema,
    graphiql: true
}));
app.use('/graphql/user', isValidUser, nohttp, nocache, graphlHTTP({
    schema: UserSchema,
    graphiql: true
}));
app.use('/graphql/ticket', isValidUser, nohttp, nocache, graphlHTTP({
    schema: TicketSchema,
    graphiql: true
}));
var phone = require('./routes/phone');
app.use('/api/login/phone', isCsrfValid, nohttp, nocache, phone);
var user = require('./routes/user');
app.use('/api/user', isValidUser, isCsrfValid, nohttp, nocache, user);

var server = app.listen(process.env.PORT || 80, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
});
server.timeout = parseInt(process.env.DEFAULT_TIMEOUT);