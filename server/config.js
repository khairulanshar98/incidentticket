var env = require('node-env-file');
env('.env');

module.exports = {
  google: {
    clientID: process.env.CLIENTID,
    clientSecret: process.env.CLIENTSECRET,
    callbackURL: process.env.CALLBACKURL
  },
  facebook: {
    clientID: process.env.APP_ID,
    clientSecret: process.env.APP_SEC,
    callbackURL: process.env.FBCALLBACKURL
  },
  phone: {
    appId: process.env.APP_ID,
    clientSecret: process.env.APP_KIT,
  },
};
