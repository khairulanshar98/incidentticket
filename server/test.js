const Tree = require("../lib/tree");
const MyArray = require("../lib/array");
const MyArray2 = require("../lib/array2");
const arr1 = new MyArray();
arr1.mypush("a")
arr1.mypush("b")
arr1.mypush("c")
arr1.mypush("d")
const arr2 = new MyArray2();
arr2.mypush(1)
arr2.mypush(2)
arr2.mypush(3)
arr2.mypush(4)
const reducer = (accumulator, currentValue) => accumulator + currentValue;
console.log(arr1, arr1.convert());
console.log(arr2);
console.log(arr1.myreduce(reducer), arr2.myreduce(reducer));



var tree = new Tree();
while (!tree.array || tree.array.length <= 20) {
    var x = Math.floor((Math.random() * 100) + 1);
    tree.addNode(x);
}
tree.addNode(56);
tree.addNode(56);
tree.addNode(56);
console.log(tree.array)
var desc = tree.order('desc');
console.log(desc)
var asc = tree.order('asc');
console.log(asc)
var index = 21;
var rootFind = tree.find(tree.array[index]);
console.log(tree.array[index], rootFind);
debugger