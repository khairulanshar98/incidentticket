var mongoose = require('mongoose');
const env = require('node-env-file');
env('.env');
var UserSchema = require('../schemas/UserSchema');
UserSchema.index({ id: 1 });
UserSchema.index({ status: 1 });
UserSchema.index({ level: 1 });
UserSchema.index({ mobileNo: 1 });
module.exports = mongoose.model('User', UserSchema);