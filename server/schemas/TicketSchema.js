var mongoose = require("mongoose");
const Schema = mongoose.Schema;
var TicketSchema = new Schema({
  title: { type: String, trim: true, default: "" },
  description: { type: String, trim: true, default: "" },
  status: { type: String, default: "Unassigned", trim: true },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  createdAt: { type: Date, default: Date.now },
  assignedTo: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  assignedAt: { type: Date, default: null },
  acknowledgedAt: { type: Date, default: null },
  resolvedAt: { type: Date, default: null },
});

/*
status: Unassigned, Assigned, Acknowledged, Resolved
*/

module.exports = TicketSchema;
