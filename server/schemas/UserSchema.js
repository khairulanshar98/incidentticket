var mongoose = require("mongoose");
const Schema = mongoose.Schema;
var UserSchema = new Schema({
  id: String,
  mobileNo: { type: String, default: "" },
  status: { type: Number, default: 1 },
  level: { type: Number, default: 4 },
  promotedBy: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
});

/*
User status: either 0 or 1. 0 is locked or not active or disabled or can not entry data.
User level:
- level 2:  User
- level 1:  Admin
- level 0:  Super Admin
*/
UserSchema.methods.canEntryData = function () {
  return this.status && this.level <= 2
}
UserSchema.methods.canPromoteTo1 = function () {
  return this.status && this.level <= 1
}
UserSchema.methods.canBePromotedTo1 = function () {
  return this.status && this.level > 1
}
UserSchema.methods.isAdmin = function () {
  return this.status && this.level <= 1
}
UserSchema.methods.isSuperAdmin = function () {
  return this.status && this.level == 0
}
module.exports = UserSchema;
