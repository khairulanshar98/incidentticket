var express = require('express');
var router = express.Router();
var uuidv4 = require('uuid/v4');
var env = require('node-env-file');
env('.env');
var { createToken, createUser, phoneVerify, getUserById, cookieObject } = require('../lib/admin.js');


router.post('/', async (req, res, next) => {
    const { token } = req.body;
    if (!token) return res.status(403).send({
        status: 'ERROR',
        message: "insufficient permission."
    });
    var { error, data } = await phoneVerify(token);
    if (error) return res.status(403).send({
        status: 'ERROR',
        message: error.message,
        error: error
    });
    if (data) {
        try {
            var user = await getUserById(data.id);
            if (!user.data) {
                data.level = 2;
                data.status = 1;
                data.mobileNo = uuidv4();
                data.mobileNo = data.mobileNo.replace(/-/g, '').substr(0, 8);
                user = await createUser(data);
            }
            if (user.data) {
                let obj = user.data;
                try {
                    obj = user.data.toObject();
                } catch (e) { }
                var csrf = uuidv4();
                obj.csrf = csrf.replace(/-/g, '');
                const token = createToken(obj);
                res.cookie(process.env.COOKIE, token, cookieObject());
                res.setHeader('csrf', obj.csrf);
                return res.json({ status: 'OK', user: obj, csrf: obj.csrf });
            }
        } catch (e) {
            res.status(403).send({
                status: 'ERROR',
                message: e.message,
                error: e
            });
        }
    }
});


module.exports = router;