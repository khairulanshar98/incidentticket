var express = require('express');
var router = express.Router();
const env = require("node-env-file");
env(".env");
var { getToken, updateUser, getUserById, searchUser } = require('../lib/admin.js');

router.put('/promoteto1', async (req, res, next) => {
    if (!req.body.id)
        return res.status(403).send({
            status: 'ERROR',
            message: "Invalid data."
        });
    var targetUser = await getUserById(req.body.id);
    if (targetUser.error || !targetUser.data.id)
        return res.status(403).send({
            status: 'ERROR',
            message: "Invalid data."
        });

    let token = getToken(req, process.env.COOKIE);
    let obj = token.payload;
    let user = await getUserById(obj.id);
    if (user.error)
        return res.status(403).send({
            status: 'ERROR',
            message: "Invalid data."
        });
    if (user.data && user.data.canPromoteTo1(user.data) && targetUser.data.canBePromotedTo1(targetUser.data)) {
        targetUser.data = targetUser.data.toObject();
        await updateUser(targetUser.data.id, {
            level: 1,
            status: 1,
            promotedBy: user.data._id,
        });
        targetUser = await getUserById(req.body.id);
        return res.json({ status: 'OK', data: targetUser.data, message: "Success" });
    }
    return res.status(403).send({
        status: 'ERROR',
        message: "Invalid Role."
    });
});

module.exports = router;