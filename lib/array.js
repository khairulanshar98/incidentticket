function MyArray() {
    this.length = 0;
}
//MyArray.prototype = Array.prototype;
MyArray.prototype.mypush = function (val) {
    this[this.length] = val;
    this.length++;
};
MyArray.prototype.convert = function () {
    const arr = [];
    Object.keys(this).map((item, i) => {
        if (item != "length") {
            arr[arr.length] = this[item + ""];
        }
    })
    return arr;
}
MyArray.prototype.myreduce = function (obj, accumulator) {
    Object.keys(this).map((item, i) => {
        if (item != "length") {
            if (!accumulator) accumulator = this[item + ""];
            else accumulator = obj(accumulator, this[item + ""]);
        }
    })
    return accumulator;
}
module.exports = MyArray;