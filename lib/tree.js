function Tree() {
    this.value = null;
    this.mode = [];
    this.index = [];
}
Tree.prototype.addNode = function (n, index) {
    if (index == null) {
        if (this.length == null) this.length = 0
        this.length++;
        if (!this.array) this.array = []
        this.array.push(n)
        index = this.length - 1;
    }
    if (this.value == null) {
        this.value = n;
        this.index = index;
        this.mode.push(index);
    } else {
        if (n < this.value) {
            if (this.left == null) {
                this.left = new Tree();
            }
            this.left.addNode(n, index)
        } else if (n > this.value) {
            if (this.right == null) {
                this.right = new Tree();
            }
            this.right.addNode(n, index)
        } else {
            this.mode.push(index);
        }
    }
}
Tree.prototype.find = function (n) {
    if (this.value == n) {
        return { value: this.value, index: this.index, mode: this.mode };
    } else {
        if (n < this.value) {
            if (this.left != null) {
                return this.left.find(n);
            }
        } else if (n > this.value) {
            if (this.right != null) {
                return this.right.find(n);
            }
        }
    }
}
Tree.prototype.order = function (type) {
    if (!type) type = 'asc';
    type = type.toLowerCase();
    if (type != 'asc' && type != 'desc') type = 'asc';
    var result = [];
    var child1 = 'left';
    var child2 = 'right';
    if (type == "desc") {
        var child1 = 'right';
        var child2 = 'left';
    }
    if (this[child1] != null) {
        var res = this[child1].order(type);
        result = res.concat(result)
    }
    for (var i = 0; i < this.mode.length; i++) {
        result.push(this.value)
    }
    if (this[child2] != null) {
        var res = this[child2].order(type);
        result = result.concat(res)
    }
    return result;
}
module.exports = Tree;